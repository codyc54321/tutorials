package main

import "fmt"

func fibbonacci() func() int {
    current_index := 0
    sum := 0
    var num_one_back int
    var num_two_back int
    return func() int {
        // fmt.Println("current_index:")
        // fmt.Println(current_index)
        fmt.Println("sum:")
        fmt.Println(sum)

        if current_index == 0 {
            // do nothing, sum is already 0
        } else if current_index == 1 {
            sum = 1
            num_one_back = 1
            num_two_back = 0
        } else {
            sum = num_two_back + num_one_back
            num_two_back = num_one_back
            num_one_back = sum
        }

        current_index += 1

        return sum
    }

}


func main() {
  fmt.Println("hello world")
  f := fibbonacci()
  for i:= 0; i < 10; i++ {
    //   fmt.Println( f() )
      f()
  }

}

