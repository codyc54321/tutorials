
# data volumes are a specially-designated directory within one or more containers that bypasses the Union File System
# https://docs.docker.com/engine/reference/glossary/#union-file-system

# Data volumes can be shared and reused among containers

# add a data volume
docker run -d -P --name web -v /webapp training/webapp python app.py
# -d: --detach, Run container in background and print container ID
# -P: --publish-all, Publish all exposed ports to random ports
# -v: --volume, Bind mount a volume

# now look at the "Mounts" portion to see:
docker inspect web

docker rm -f web

# mount a local volume
docker run -d -P --name web -v ~/src/webapp:/webapp training/webapp python app.py

docker rm -f web

# make the volume read only
docker run -d -P --name web -v ~/src/webapp:/webapp:ro training/webapp python app.py

# rm web

# install flocker

# tutorial has to be brokem, or it wouldn't be shiny new tech!

# neither commands work on mac
docker run -d -P \
  --volume-driver=flocker \
  -v my-named-volume:/webapp \
  --name web training/webapp python app.py

# or make volume first as its own step
docker volume create -d flocker -o size=20GB my-named-volume
docker run -d -P \
  -v my-named-volume:/webapp \
  --name web training/webapp python app.py

# try this
docker volume create --name=codyvolume1 --opt o=size=100m

# add a single file
docker run --rm -it -v ~/.bash_history:/root/.bash_history ubuntu /bin/bash

# now do history in the container
# exit, and do history on local machine. it tracked it in both directions
