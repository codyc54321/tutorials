//
//  ToDoItem.swift
//  ClearStyle
//
//  Created by Cody Childers on 6/3/17.
//  Copyright © 2017 Cody Childers. All rights reserved.
//

import UIKit

class ToDoItem: NSObject {
    var text: String
    var completed: Bool
    
    init(text: String) {
        self.text = text
        self.completed = false
    }

}
