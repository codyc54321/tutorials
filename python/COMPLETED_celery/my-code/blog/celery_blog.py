import requests

from celery_config import celery_app


@celery_app.task
def fetch_url(url):
    resp = requests.get(url)
    print(url)
    print(resp.status_code)

def func(urls):
    for url in urls:
        fetch_url.delay(url)

if __name__ == "__main__":
    func(["http://google.com", "http://amazon.in", "http://facebook.com", "http://twitter.com", "http://alexa.com"])
