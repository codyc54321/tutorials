"Jose " <> last_name = "Jose Valim"
IO.puts last_name

# Causes error because can't match
# "Jose " <> last_name = " Valim"
# IO.puts last_name

data = ["Elixir", "Valim"]
IO.puts data

# unpack a list
[lang, author] = data
IO.puts "#{lang}, #{author}"



defmodule OutdatedAccount do
    def run_transaction_outdated(balance, amount, type) do
        if type == :deposit do
            balance + amount
        else
            balance - amount
        end
    end
end

OutdatedAccount.run_transaction_outdated(1000, 50, :deposit) |> IO.puts
OutdatedAccount.run_transaction_outdated(1000, 50, :withdrawal) |> IO.puts

# proper elixir below...
defmodule Account do
    def run_transaction(balance, amount, :deposit) do
        balance + amount
    end

    def run_transaction(balance, amount, :withdrawal) do
        balance - amount
    end
end

IO.puts "\nLook at how awesome this is...the first arg is passed through twice"
1000 |> Account.run_transaction(50, :deposit)
     |> Account.run_transaction(20, :withdrawal)
     |> IO.puts
