defmodule Language do
  def print_list([head | tail]) do
    IO.puts head
    print_list(tail)
  end

  def print_list([]) do
  end

end


mylist = ['r', 'e', 'c', 'u', 'r', 's', 'i', 'o', 'n']

Language.print_list(mylist)
