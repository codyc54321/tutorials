use Mix.Config

# In this file, we keep production configuration that
# you likely want to automate and keep it away from
# your version control system.
#
# You should document the content of this
# file or create a script for recreating it, since it's
# kept out of version control and might be hard to recover
# or recreate for your teammates (or you later on).
config :phoenix_chat, PhoenixChat.Endpoint,
  secret_key_base: "oZ3L7jBPFCmQO775Hv1vRjCe0itr2s0CIo2GdOZCN/VcsuIQ4Bw6/jBuuZC+NIBe"

# Configure your database
config :phoenix_chat, PhoenixChat.Repo,
  adapter: Ecto.Adapters.Postgres,
  username: "postgres",
  password: "postgres",
  database: "phoenix_chat_prod",
  pool_size: 20

  
