#!/bin/bash

X=("django" "bs4")

# declare a func make_file
#  which checks if a file doesn't exist (!  means not: if not a file (-f) exists at $HOME/$1 ($1 = the first arg) then 
# make $HOME/$1
# end the if statement
# end the func

make_file() {
  if [ ! -f "$HOME/$1" ]; then
    touch "$HOME/$1"
  fi
}


make_file fake/thisone
