run a script by calling it:

  ./my_script.sh

if you can't tab complete (hit tab and it shows the full name) make it runnable:

  chmod +x my_script.sh # add execute permission

make sure each script starts with 

  #!/bin/bash

this tells machine to call it with the bash program (so it runs as bash script)
http://unix.stackexchange.com/questions/87560/does-the-shebang-determine-the-shell-which-runs-the-script

you can prove it by listing out the file:

  cat /bin/bash

it's an unreadable binary

any command you wanna read is best to do "man" before googling:

  man cat

you can leave here by hitting "q" for quit


