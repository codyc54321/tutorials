#!/bin/bash

# http://www.cyberciti.biz/faq/bash-for-loop-array/

arrayName=( "one", "two", "three" );
array=( one two three )
files=( "/etc/passwd" "/etc/group" "/etc/hosts" )
limits=( 10, 20, 26, 39, 48)

for i in "${arrayName[@]}"
do
   :
   echo $i;
done

printf "%s\n" "${array[@]}"
printf "%s\n" "${files[@]}"
printf "%s\n" "${limits[@]}"