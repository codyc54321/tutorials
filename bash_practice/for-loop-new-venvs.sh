#!/bin/bash

# http://www.cyberciti.biz/faq/bash-for-loop-array/

projects=( "PythonPortal" "DevopsServices" );


for project in "${projects[@]}"
do
   :
   git clone https://github.homedepot.com/Devops-Automation/$project.git
   source /usr/local/bin/virtualenvwrapper.sh && mkvirtualenv $project
   ~/.virtualenvs/python_portal/bin/pip install \
   --trusted-host artifactory-qa.homedepot.com -r ~/work_projects/$project/requirements.txt

done
