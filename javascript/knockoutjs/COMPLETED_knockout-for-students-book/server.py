import json
from flask import Flask, jsonify, request
from flask_cors import CORS, cross_origin

app = Flask(__name__)
CORS(app)


@app.route('/')
def hello():
    return 'Hello, world'


# http://stackoverflow.com/questions/13081532/how-to-return-json-using-flask-web-framework
@app.route('/data')
def data():
    data = {
        'firstName': 'Cody',
        'lastName': 'Childers',
        'activities': [
            'Golf',
            'Kayaking',
            'Web development',
        ],
        'favoriteHobby': 'Golf',
    }

    return jsonify(**data)


@app.route('/receive-data', methods=['POST'])
def receive_data():
    # import ipdb; ipdb.set_trace()
    # data = request.get_json(force=True)
    print(request.data)
    request_json = json.loads(request.data)
    print(request_json)
    return jsonify(status=200, message='yep')


if __name__ == "__main__":
    app.run()
