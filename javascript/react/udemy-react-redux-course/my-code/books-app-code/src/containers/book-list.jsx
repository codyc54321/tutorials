import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { selectBook } from '../actions/index';


class BookList extends Component {
    renderList() {
        return this.props.books.map((book) => {
            return (
                <li
                  key={book.title}
                  onClick={() => this.props.selectBook(book)}
                  className="list-group-item">
                    {book.title}
                </li>
            );
        });
    }

    render() {
        return (
            <ul className="list-group col-sm-4">
                {this.renderList()}
            </ul>
        )
    }
}


function mapStateToProps(state) {
    return {
        books: state.books
    };
}

// anything returned from this function will end up as props on the Booklist container
function mapDispatchToProps(dispatch) {
    // whenever selectBook is called, the result should be passed to all reducers
    // that's what bindActionCreators does
    return bindActionCreators({selectBook: selectBook}, dispatch);
}


export default connect(mapStateToProps, mapDispatchToProps)(BookList);
