import axios from 'axios';

const OPENWEATHER_API_KEY = '390397ff140a629518e6e1d4fbd72e61'

export const FETCH_WEATHER = 'FETCH_WEATHER';
export const OPENWEATHER_ROOT_URL = `http://api.openweathermap.org/data/2.5/forecast?appid=${OPENWEATHER_API_KEY}`


export function fetchWeather(city) {
    const url = `${OPENWEATHER_ROOT_URL}&q=${city},us`;
    const request = axios.get(url);
    
    return {
        type: FETCH_WEATHER,
        payload: request
    }
}
