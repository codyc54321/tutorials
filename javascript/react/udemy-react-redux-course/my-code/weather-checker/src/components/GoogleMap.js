import React from 'react';

import MyComponent from '../utils/MyComponent';


export default class GoogleMap extends MyComponent {

    componentDidMount() {
        new google.maps.Map(
            this.refs.map,
            {
                zoom: 12,
                center: {
                    lat: this.props.lat,
                    lng: this.props.lon
            }
        });
    }

    render() {
        return <div ref="map" />;
    }
}
