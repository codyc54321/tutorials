import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { fetchWeather } from '../actions/index';
import MyComponent from '../utils/MyComponent';


class SearchBar extends MyComponent {

    constructor(props) {
        let custom_methods = ['onInputChange', 'handleFormSubmit'];
        super(props, custom_methods);
        this.state = {term: ''};
    }

    handleFormSubmit(event) {
        event.preventDefault();
        this.props.fetchWeather(this.state.term);
        this.setState({term: ''});
    }

    onInputChange(event) {
        this.setState({term: event.target.value})
    }

    render() {
        return (
            <form
              onSubmit={this.handleFormSubmit}
              className="input-group">
                <input
                  placeholder="Get a five-day forecase in your favorite cities"
                  className="form-control"
                  value={this.state.term}
                  onChange={this.onInputChange}
                  />
                <span className="input-group-btn">
                    <button type="submit" className="btn btn-secondary">Submit</button>
                </span>
            </form>
        )
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({ fetchWeather }, dispatch);
}

export default connect(null, mapDispatchToProps)(SearchBar);
