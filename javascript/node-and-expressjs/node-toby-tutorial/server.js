var express = require('express');
var app = express();
var morgan = require('morgan');
var bodyParser = require('body-parser');

var port = process.env.PORT || 3000;

app.use(morgan('dev'));
app.use(bodyParser.json());
app.use(express.static('public'));

app.get('/greeting', function(req, res) {
    res.json({greeting: 'Howdy World!'});
});

app.post('/greeting', function(req, res) {
    var name = req.body.name;
    res.json({greeting: 'Howdy ' + name + '!' });
});

app.listen(port, function(err) {
    if(err) {
        console.log('Unable to start server:');
        console.log(err);
    }
    else {
        console.log('Server listening on port ', port);
    }
});
