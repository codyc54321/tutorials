import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Location } from '@angular/common';

import 'rxjs/add/operator/switchMap';
import { FlashMessagesService } from 'angular2-flash-messages';

import { Hero, HeroIds }         from './hero';
import { HeroService } from './hero.service';


@Component({
  moduleId: module.id,
  selector: 'my-hero-detail',
  templateUrl: './static/templates/hero-detail.component.html',
  styleUrls: ['./static/css/hero-detail.component.css'],
})
export class HeroDetailComponent implements OnInit {
    @Input() hero: Hero;
    hero_ids: Array<HeroIds> = [];

    constructor(
        private heroService: HeroService,
        private route: ActivatedRoute,
        private location: Location,
        private router: Router,
        private _flashMessagesService: FlashMessagesService
    ) { }

    ngOnInit(): void {
        this.route.params
            .switchMap((params: Params) => this.heroService.getHero(+params['id']))
            .subscribe(hero => this.hero = hero);
    }

    goBack(): void {
        this.location.back();
    }

    save(): void {
        this.heroService.update(this.hero)
            .then(() => this.goBack());
    }

    gotoDetail(hero_id: string): void {
        this.heroService.getHeroIds()
          .then(
            result => {
                if ( result.includes(+hero_id) ) {
                    this.router.navigate(['/detail', hero_id]);
                } else {
                    this._flashMessagesService.show("Please pick a valid hero ID");
                }
            }
        );
    }
}
