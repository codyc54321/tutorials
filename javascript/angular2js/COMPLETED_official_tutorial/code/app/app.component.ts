import { Component } from '@angular/core';


@Component({
    moduleId: module.id,
    selector: 'my-app',
    template: `
    <flash-messages></flash-messages>
    <h1>{{title}}</h1>
    <nav>
        <a routerLink="/dashboard" routerLinkActive="active">Dashboard</a>
        <a routerLink="/heroes" routerLinkActive="active">Heroes</a>
    </nav>
    <router-outlet></router-outlet>
    `,
    styleUrls: ['./static/css/app.component.css'],
})
export class AppComponent {
    title = 'Tour of Heroes';
}
