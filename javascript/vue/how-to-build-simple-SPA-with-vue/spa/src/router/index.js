import Vue from 'vue'
import Router from 'vue-router'
import Hello from '@/components/Hello'
import About from '../components/About'
import Param from '@/components/Param';

Vue.use(Router)


export default new Router({
    routes: [
        {
            path: '/',
            component: Hello
        },
        {
            path: '/about',
            component: About
        },
        {
            path: '/param',
            component: Param
        }
    ],
    mode: 'history'
})
